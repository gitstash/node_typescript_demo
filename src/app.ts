import {} from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import cameraApi from './components/camera/cameraApi';

// configure the express app
export const getApp = () => {
  const app = express();
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use([bodyParser.json(), bodyParser.urlencoded({ extended: true }), cors()]);
  // endpoint configuration
  app.use('/api/camera', cameraApi);
  app.get('/', (_, res) => {
    res.send(`to use our api go to the endpoint "/api/help"`);
  });

  // testing the api
  app.get('/api/ping', (_, res) => {
    res.json({ ok: true });
  });
  return app;
};
