import mongoose, { Schema, Document } from 'mongoose';

export interface Camera extends Document {
  intId: number;
  navigation: [
    {
      direction: string;
      cameraIntId: number;
      meters: number;
    },
  ];
}

const CameraSchema: Schema = new Schema({
  intId: { type: Number, required: true, unique: true },
  navigation: {
    type: [
      {
        direction: String,
        cameraIntId: Number,
        meters: Number,
      },
    ],
  },
});

// Export the model
export default mongoose.model<Camera>('Camera', CameraSchema, 'Camera');
