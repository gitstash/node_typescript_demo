import { expect } from 'chai';
import request from 'supertest';
import { getApp } from '../app';

describe('Check ping', () => {
  it('works', async () => {
    const app = getApp();
    const res = await request(app).get('/api/ping');
    const { ok } = res.body;
    expect(res.status).to.equal(200);
    expect(ok).to.equal(true);
  });
});
