export const PORT: string = process.env.PORT || '3000';
export const DB_URI: string = process.env.DB_URI || 'mongodb://mongo:27017/navi';
