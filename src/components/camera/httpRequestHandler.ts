import { httpResponseType } from './types';
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function httpResponse(res: any, { success, httpStatus, data, error }: httpResponseType) {
  success ? res.status(httpStatus || 200).json(data) : res.status(httpStatus || 500).end(error);
}
