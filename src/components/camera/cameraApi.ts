import express from 'express';
import { createCamera, getCameras } from './cameraDal';
import { httpResponse } from './httpRequestHandler';
import { httpResponseType } from './types';
const router = express.Router();

// GET CAMERA
router.get('/', async (_, res) => {
  const result = await getCameras();
  httpResponse(res, result);
});

// GET CAMERA BY INDEX
router.get('/:index', (_, res) => {
  res.send([{ camera: 1 }] || []);
});

// CREATE CAMERA
router.post('/', async (req, res) => {
  const contextObject = {
    intId: req.body.intId || null,
    navigation: req.body.navigation,
  };
  const result: httpResponseType = await createCamera(contextObject);
  httpResponse(res, result);
});

// EDIT CAMERA
router.put('/', (_, res) => {
  res.send([{ camera: 1 }] || []);
});

export default router;
