export type httpResponseType = {
  success: boolean;
  httpStatus: number;
  error?: Error;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data?: any;
};

enum DirectionType {
  goStraight,
  turnLeft,
  turnRight,
  GoBack,
}

export type CameraType = {
  intId: number;
  navigation?: {
    camera: CameraType;
    direction: DirectionType;
    meters: number;
  }[];
};
