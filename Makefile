# Drop fdjFoot database
drop-db:  # adapt this as your needs
	# docker exec -it mongo \
	# mongo --eval "db.dropDatabase()" fdjFoot;

# load fixtures
load-fixtures: # adapt this as your needs
	# curl -o src/fixtures/players.json http://ingdownload.free.fr/fixtures/players.json && \
	# curl -o src/fixtures/teams.json http://ingdownload.free.fr/fixtures/teams.json && \
	# docker cp src/fixtures/teams.json mongo:/data && \
	# docker cp src/fixtures/players.json mongo:/data;

# import fixtures in the database
import-fixtures: # adapt this as your needs
	# echo "mongoimport --db fdjFoot --collection teams --file ./data/teams.json" | docker exec -i mongo /bin/bash && \
	# echo "mongoimport --db fdjFoot --collection players --file ./data/players.json" | docker exec -i mongo /bin/bash;

# reset DB with fixutres
reset-db: # adapt this as your needs
	# make drop-db && make load-fixtures && make import-fixtures;