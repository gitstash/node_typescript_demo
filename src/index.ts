import { PORT, DB_URI } from 'src/configuration';
import { getApp } from 'src/app';
import {} from 'dotenv';
import connect from './connect';

// run the api
const startServer = async () => {
  try {
    const app = getApp();
    connect(DB_URI);
    app.listen(PORT, () => {
      console.log(`api running on port ${PORT}`);
    });
  } catch (error) {
    console.error(error);
  }
};
startServer();
