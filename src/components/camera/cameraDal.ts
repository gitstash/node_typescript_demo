import { CameraType } from './types';
import Camera from './Camera';

export async function createCamera(camera: CameraType) {
  try {
    const newCamera = new Camera(camera);
    await newCamera.save();
    return { success: true, httpStatus: 200, data: newCamera };
  } catch (e) {
    console.error(e);
    return { success: false, httpStatus: e.status, error: e.message };
  }
}

export async function getCameras() {
  try {
    const cameras = await Camera.find();
    return { success: true, httpStatus: 200, data: cameras };
  } catch (e) {
    console.error(e);
    return { success: false, httpStatus: e.status, error: e.message };
  }
}
